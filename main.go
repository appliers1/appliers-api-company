package main

import (
	"appliers/api-company/database"
	"appliers/api-company/database/migration"
	"appliers/api-company/database/seeder"
	"appliers/api-company/internal/http"
	"appliers/api-company/internal/http/middleware"
	"flag"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

// load env configuration
func init() {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
}

func main() {

	database.CreateConnection()
	db := database.GetConnection()

	var m string // for check migration

	flag.StringVar(
		&m,
		"migrate",
		"run",
		`this argument for check if user want to migrate table, rollback table, or status migration

to use this flag:
	use -migrate=migrate for migrate table
	use -migrate=rollback for rollback table
	use -migrate=status for get status migration`,
	)
	flag.Parse()

	if m == "migrate" {
		migration.Migrate()
		seeder.Seed(db)
		return
	} else if m == "rollback" {
		migration.Rollback()
		return
	} else if m == "status" {
		migration.Status()
		return
	}

	e := echo.New()

	middleware.Init(e)
	http.Init(e, db)

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
