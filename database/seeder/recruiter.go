package seeder

import (
	"appliers/api-company/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func recruiterTableSeeder(conn *gorm.DB) {

	var recruiters = []domain.Recruiter{
		{
			UserID:    2,
			CompanyID: 1,
			Name:      "Nadia Rahma",
		},
		{
			UserID:    3,
			CompanyID: 1,
			Name:      "Rafi Ahmad",
		},
		{
			UserID:    4,
			CompanyID: 2,
			Name:      "Dita Fitriani",
		},
		{
			UserID:    5,
			CompanyID: 2,
			Name:      "Taqy Malik",
		},
	}

	var count int64
	if err := conn.Model(&domain.Recruiter{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data recruiters, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data recruiters, table not empty")
		} else {
			if err := conn.Create(&recruiters).Error; err != nil {
				log.Printf("cannot seed data recruiters, with error %v\n", err)
			}
			log.Println("success seed data recruiters")
		}
	}
}
