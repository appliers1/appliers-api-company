package seeder

import (
	"appliers/api-company/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func companyAddressTableSeeder(conn *gorm.DB) {

	var companyAddresses = []domain.CompanyAddress{
		{
			CompanyID:  1,
			Address:    "Nissi Bintaro Campus, 5th floor. Jalan Tegal Rotan No 78, Bintaro Sektor 9 Tangerang Selatan",
			PostalCode: "15413",
			Telp:       "(021) 22213077",
		},
		{
			CompanyID:  1,
			Address:    "71 Robinson road 13th floor. Singapore",
			PostalCode: "068895",
			Telp:       "+65 8462 4452",
		},
		{
			CompanyID:  1,
			Address:    "Bhiraj Tower @BITEC. 23rd floor, 4345 Sukhumvit Road Bangna Sub-District, Bangna District, Thailand",
			PostalCode: "",
			Telp:       "+662 0175180",
		},
		{
			CompanyID:  2,
			Address:    "Jalan Raya Tidar, Nomor 23, Karangbesuki, Kota Malang, Jawa Timur.",
			PostalCode: "65146",
			Telp:       "",
		},
	}

	var count int64
	if err := conn.Model(&domain.CompanyAddress{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data companyAddresses, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data companyAddresses, table not empty")
		} else {
			if err := conn.Create(&companyAddresses).Error; err != nil {
				log.Printf("cannot seed data companyAddresses, with error %v\n", err)
			}
			log.Println("success seed data companyAddresses")
		}
	}
}
