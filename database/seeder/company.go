package seeder

import (
	"appliers/api-company/internal/model/domain"
	"log"

	"gorm.io/gorm"
)

func companyTableSeeder(conn *gorm.DB) {

	var companies = []domain.Company{
		{
			Name:           "Data On",
			Service:        "ERP & HRIS system",
			About:          "DataOn is the leading provider of HR, ERP, and Accounting technology solutions trusted since 1999. We believe a comprehensive system gives people fast and increase the effectiveness to achieving the organizational goals.",
			Logo:           "dataon-logo.jpg",
			TotalEmployees: 2000,
			Website:        "dataon.com",
			Email:          "info@dataon.com",
		},
		{
			Name:           "Alterra Academy",
			Service:        "Education & Tech Training Partner",
			About:          "Alterra Academy is an Indonesian coding bootcamp for everyone, both with IT & non-IT background to become the best quality Programmer in the Industry today.",
			Logo:           "alterra-logo.jpg",
			TotalEmployees: 1000,
			Website:        "academy.alterra.id",
			Email:          "info@academy.alterra.id",
		},
		{
			Name:           "Mbakul",
			Service:        "Food Supplier",
			About:          "Just stay at home, we will deliver the order",
			Logo:           "mbakul-logo.jpg",
			TotalEmployees: 20,
			Website:        "mbakul.com",
			Email:          "info@mbakul.com",
		},
	}

	var count int64
	if err := conn.Model(&domain.Company{}).Count(&count).Error; err != nil {
		log.Printf("cannot seed data companies, with error %v\n", err)
	} else {
		if count > 0 {
			log.Printf("cannot seed data companies, table not empty")
		} else {
			if err := conn.Create(&companies).Error; err != nil {
				log.Printf("cannot seed data companies, with error %v\n", err)
			}
			log.Println("success seed data companies")
		}
	}
}
