package constant

import (
	"gorm.io/gorm"
)

var BaseUrl = "http://13.250.105.254" // API Ingress nya

var (
	UserServiceUrl        = BaseUrl // + ":8001"
	CompanyServiceUrl     = BaseUrl // + ":8002"
	ApplicantServiceUrl   = BaseUrl // + ":8003"
	JobServiceUrl         = BaseUrl // + ":8004"
	ApplicationServiceUrl = BaseUrl // + ":8005"
)

const CONTEXT_JWT_PAYLOAD_KEY = "jwt_payload_key"

/* CONTENT TYPE */
const ContentTypeApplicationJson = "application/json"

var RecordNotFound = gorm.ErrRecordNotFound

type UserRole string
type UserStatus string

var AllRole = map[UserRole]bool{
	ROLE_ADMIN:     true,
	ROLE_APPLICANT: true,
	ROLE_COMPANY:   true,
}

const (
	ROLE_ADMIN     UserRole = "admin"
	ROLE_APPLICANT UserRole = "applicant"
	ROLE_COMPANY   UserRole = "company"
)

const (
	STATUS_ACTIVE    UserStatus = "active"
	STATUS_PENDING   UserStatus = "pending"
	STATUS_SUSPENDED UserStatus = "suspended"
	STATUS_BANNED    UserStatus = "banned"
)
