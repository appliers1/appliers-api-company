package repository

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type CompanyRepository interface {
	Save(ctx context.Context, company *Company) (*Company, error)
	Update(ctx context.Context, company *Company, companyId uint) (*Company, error)
	Delete(ctx context.Context, companyId uint) error
	FindById(ctx context.Context, companyId uint) (*Company, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]Company, *dto.PaginationInfo, error)
	CountById(ctx context.Context, companyId uint) (int, error)
}

type CompanyRepositoryImplSql struct {
	DB *gorm.DB
}

func NewCompanyRepositoryImplSql(db *gorm.DB) CompanyRepository {
	return &CompanyRepositoryImplSql{
		DB: db,
	}
}

func (repository *CompanyRepositoryImplSql) Save(ctx context.Context, company *Company) (*Company, error) {
	err := repository.DB.WithContext(ctx).Save(company).Error
	return company, err
}

func (repository *CompanyRepositoryImplSql) Update(ctx context.Context, company *Company, companyId uint) (*Company, error) {
	err := repository.DB.WithContext(ctx).Model(company).Where("id = ?", companyId).Updates(&company).Error
	updatedCompany := Company{}
	repository.DB.WithContext(ctx).First(&updatedCompany, companyId)
	return &updatedCompany, err
}

func (repository *CompanyRepositoryImplSql) Delete(ctx context.Context, companyId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&Company{}, companyId).Error
	return err
}

func (repository *CompanyRepositoryImplSql) FindById(ctx context.Context, companyId uint) (*Company, error) {
	var company = Company{}
	err := repository.DB.WithContext(ctx).Preload("CompanyAddresses").Preload("Recruiters").First(&company, companyId).Error
	if err == nil {
		return &company, nil
	} else {
		return nil, err
	}
}

func (repository *CompanyRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]Company, *dto.PaginationInfo, error) {
	var companies []Company
	var count int64

	query := repository.DB.WithContext(ctx).Model(&Company{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(name) LIKE ? ", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	// err := query.Preload("CompanyAddresses").
	// 	Preload("Recruiters", func(tx *gorm.DB) *gorm.DB {
	// 		return tx.Select("id", "company_id","name", "user_id")
	// 	}).
	// 	Limit(limit).Offset(offset).Find(&companies).Error
	err := query.Preload("CompanyAddresses").Preload("Recruiters").Limit(limit).Offset(offset).Find(&companies).Error

	return companies, dto.CheckInfoPagination(p, count), err
}

func (repository *CompanyRepositoryImplSql) CountById(ctx context.Context, companyId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Company{}).Where("id = ?", companyId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
