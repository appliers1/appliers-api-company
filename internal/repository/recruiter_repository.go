package repository

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type RecruiterRepository interface {
	Save(ctx context.Context, recruiter *Recruiter) (*Recruiter, error)
	Update(ctx context.Context, recruiter *Recruiter, recruiterId uint) (*Recruiter, error)
	Delete(ctx context.Context, recruiterId uint) error
	FindById(ctx context.Context, recruiterId uint, companyId uint) (*Recruiter, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination, companyId uint) ([]Recruiter, *dto.PaginationInfo, error)
	CountById(ctx context.Context, recruiterId uint) (int, error)
	FindByUserId(ctx context.Context, userId uint) (*Recruiter, error)
}

type RecruiterRepositoryImplSql struct {
	DB *gorm.DB
}

func NewRecruiterRepositoryImplSql(db *gorm.DB) RecruiterRepository {
	return &RecruiterRepositoryImplSql{
		DB: db,
	}
}

func (repository *RecruiterRepositoryImplSql) Save(ctx context.Context, recruiter *Recruiter) (*Recruiter, error) {
	err := repository.DB.WithContext(ctx).Save(recruiter).Error
	return recruiter, err
}

func (repository *RecruiterRepositoryImplSql) Update(ctx context.Context, recruiter *Recruiter, recruiterId uint) (*Recruiter, error) {
	err := repository.DB.WithContext(ctx).Model(recruiter).Where("id = ?", recruiterId).Updates(&recruiter).Error
	updatedAddress := Recruiter{}
	repository.DB.WithContext(ctx).First(&updatedAddress, recruiterId)
	return &updatedAddress, err
}

func (repository *RecruiterRepositoryImplSql) Delete(ctx context.Context, recruiterId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&Recruiter{}, recruiterId).Error
	return err
}

func (repository *RecruiterRepositoryImplSql) FindById(ctx context.Context, recruiterId uint, companyId uint) (*Recruiter, error) {
	var recruiter = Recruiter{}
	err := repository.DB.WithContext(ctx).Where("company_id = ?", companyId).First(&recruiter, recruiterId).Error
	if err != nil {
		return nil, err
	}
	return &recruiter, nil
}

func (repository *RecruiterRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination, companyId uint) ([]Recruiter, *dto.PaginationInfo, error) {
	var recruiters []Recruiter
	var count int64

	query := repository.DB.WithContext(ctx).Model(&Recruiter{})
	query = query.Where("company_id = ?", companyId)

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(name) LIKE ? ", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&recruiters).Error

	return recruiters, dto.CheckInfoPagination(p, count), err
}

func (repository *RecruiterRepositoryImplSql) CountById(ctx context.Context, recruiterId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&Recruiter{}).Where("id = ?", recruiterId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *RecruiterRepositoryImplSql) FindByUserId(ctx context.Context, userId uint) (*Recruiter, error) {
	var recruiter = Recruiter{}
	err := repository.DB.WithContext(ctx).Where("user_id = ? ", userId).First(&recruiter).Error
	if err != nil {
		return nil, err
	}
	return &recruiter, nil
}
