package repository

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type CompanyAddressRepository interface {
	Save(ctx context.Context, address *CompanyAddress) (*CompanyAddress, error)
	Update(ctx context.Context, address *CompanyAddress, addressId uint) (*CompanyAddress, error)
	Delete(ctx context.Context, addressId uint) error
	FindById(ctx context.Context, addressId uint) (*CompanyAddress, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, companyId uint, p *dto.Pagination) ([]CompanyAddress, *dto.PaginationInfo, error)
	CountById(ctx context.Context, addressId uint) (int, error)
}

type CompanyAddressRepositoryImplSql struct {
	DB *gorm.DB
}

func NewCompanyAddressRepositoryImplSql(db *gorm.DB) CompanyAddressRepository {
	return &CompanyAddressRepositoryImplSql{
		DB: db,
	}
}

func (repository *CompanyAddressRepositoryImplSql) Save(ctx context.Context, address *CompanyAddress) (*CompanyAddress, error) {
	err := repository.DB.WithContext(ctx).Save(address).Error
	return address, err
}

func (repository *CompanyAddressRepositoryImplSql) Update(ctx context.Context, address *CompanyAddress, addressId uint) (*CompanyAddress, error) {
	err := repository.DB.WithContext(ctx).Model(address).Where("id = ?", addressId).Updates(&address).Error
	updatedAddress := CompanyAddress{}
	repository.DB.WithContext(ctx).First(&updatedAddress, addressId)
	return &updatedAddress, err
}

func (repository *CompanyAddressRepositoryImplSql) Delete(ctx context.Context, addressId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&CompanyAddress{}, addressId).Error
	return err
}

func (repository *CompanyAddressRepositoryImplSql) FindById(ctx context.Context, addressId uint) (*CompanyAddress, error) {
	var address = CompanyAddress{}
	err := repository.DB.WithContext(ctx).First(&address, addressId).Error
	if err == nil {
		return &address, nil
	} else {
		return nil, err
	}
}

func (repository *CompanyAddressRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, companyId uint, p *dto.Pagination) ([]CompanyAddress, *dto.PaginationInfo, error) {
	var addresss []CompanyAddress
	var count int64

	query := repository.DB.WithContext(ctx).Model(&CompanyAddress{})
	query = query.Where("company_id = ?", companyId) // TODO : ini wajib di uncommand, di command sementara untuk keperluan develpment

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(address) LIKE ? ", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&addresss).Error
	return addresss, dto.CheckInfoPagination(p, count), err
}

func (repository *CompanyAddressRepositoryImplSql) CountById(ctx context.Context, addressId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&CompanyAddress{}).Where("id = ?", addressId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
