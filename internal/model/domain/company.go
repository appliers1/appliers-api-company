package domain

import (
	"gorm.io/gorm"
)

type Company struct {
	gorm.Model
	Name             string `json:"name" gorm:"size:100;not null"`
	Service          string `json:"service" gorm:"size:500;not null"`
	About            string `json:"about" gorm:""`
	Logo             string `json:"logo" gorm:"size:100;"`
	TotalEmployees   int    `json:"total_employees" gorm:"default:0;not null"`
	Website          string `json:"website" gorm:"size:100;"`
	Email            string `json:"email" gorm:"size:200;"`
	CompanyAddresses []CompanyAddress
	Recruiters       []Recruiter
}
