package domain

type CompanyAddress struct {
	ID         uint   `json:"id" gorm:"primarykey;autoIncrement;"`
	CompanyID  uint   `json:"company_id" gorm:"not null"`
	Address    string `json:"address" gorm:"size:500;not null"`
	PostalCode string `json:"postal_code" gorm:"size:6"`
	Telp       string `json:"telp" gorm:"size:50"`
}
