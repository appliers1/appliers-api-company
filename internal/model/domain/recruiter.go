package domain

import "gorm.io/gorm"

type Recruiter struct {
	gorm.Model
	UserID    uint   `json:"user_id" gorm:"not null;uniqueIndex;"`
	CompanyID uint   `json:"company_id" gorm:"default:null;"`
	Name      string `json:"name" gorm:"size:100;not null"`
}
