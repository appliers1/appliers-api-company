package dto

/* REQUEST */
type RecruiterRequestBody struct {
	CompanyID *uint  `json:"company_id"`
	UserID    uint   `json:"user_id" gorm:"not null"`
	Name      string `json:"name" gorm:"size:100;not null"`
}

type UpdateRecruiterRequestBody struct {
	CompanyID *uint   `json:"company_id"`
	UserID    *uint   `json:"user_id"`
	Name      *string `json:"name"`
}

/* RESPONSE */
type RecruiterResponse struct {
	ID        uint   `json:"id"`
	CompanyID uint   `json:"company_id"`
	UserID    uint   `json:"user_id" gorm:"not null"`
	Name      string `json:"name" gorm:"size:100;not null"`
}
