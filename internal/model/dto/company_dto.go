package dto

import "appliers/api-company/internal/model/domain"

/* REQUEST */
type CompanyRequestBody struct {
	Name           string `json:"name"`
	Service        string `json:"service"`
	About          string `json:"about"`
	Logo           string `json:"logo"`
	TotalEmployees int    `json:"total_employees"`
	Website        string `json:"website"`
	Email          string `json:"email"`
}

type UpdateCompanyRequestBody struct {
	Name           *string `json:"name"`
	Service        *string `json:"service"`
	About          *string `json:"about"`
	Logo           *string `json:"logo"`
	TotalEmployees *int    `json:"total_employees"`
	Website        *string `json:"website"`
	Email          *string `json:"email"`
}

/* RESPONSE */
type CompanyResponse struct {
	ID               uint                    `json:"id"`
	Name             string                  `json:"name" gorm:"size:100;not null"`
	Service          string                  `json:"service" gorm:"size:500;not null"`
	About            string                  `json:"about" gorm:""`
	Logo             string                  `json:"logo" gorm:"size:100;"`
	TotalEmployees   int                     `json:"total_employees" gorm:"default:0;not null"`
	Website          string                  `json:"website" gorm:"size:100;"`
	Email            string                  `json:"email" gorm:"size:200;"`
	CompanyAddresses []domain.CompanyAddress `json:"company_addresses"`
	Recruiters       []RecruiterResponse     `json:"recruiters"`
}
