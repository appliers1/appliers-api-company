package dto

import (
	"appliers/api-company/internal/model/domain"
)

/* REQUEST */
type CompanyAddressRequestBody struct {
	CompanyID  uint   `json:"company_id"`
	Address    string `json:"address"`
	PostalCode string `json:"postal_code"`
	Telp       string `json:"telp"`
}

type UpdateCompanyAddressRequestBody struct {
	Address    *string `json:"address"`
	PostalCode *string `json:"postal_code"`
	Telp       *string `json:"telp"`
}

/* RESPONSE */
type CompanyAddressResponse struct {
	CompanyAddress domain.CompanyAddress `json:"company_address"`
}
