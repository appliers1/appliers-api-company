package factory

import (
	"appliers/api-company/internal/repository"

	"gorm.io/gorm"
)

type RecruiterFactory struct {
	CompanyRepository   repository.CompanyRepository
	RecruiterRepository repository.RecruiterRepository
}

func NewRecruiterFactory(db *gorm.DB) *RecruiterFactory {
	return &RecruiterFactory{
		CompanyRepository:   repository.NewCompanyRepositoryImplSql(db),
		RecruiterRepository: repository.NewRecruiterRepositoryImplSql(db),
	}
}
