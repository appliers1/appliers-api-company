package factory

import (
	"appliers/api-company/internal/repository"

	"gorm.io/gorm"
)

type CompanyAddressFactory struct {
	CompanyRepository        repository.CompanyRepository
	CompanyAddressRepository repository.CompanyAddressRepository
}

func NewCompanyAddressFactory(db *gorm.DB) *CompanyAddressFactory {
	return &CompanyAddressFactory{
		CompanyRepository:        repository.NewCompanyRepositoryImplSql(db),
		CompanyAddressRepository: repository.NewCompanyAddressRepositoryImplSql(db),
	}
}
