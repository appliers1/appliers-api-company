package factory

import (
	"appliers/api-company/internal/repository"

	"gorm.io/gorm"
)

type CompanyFactory struct {
	CompanyRepository   repository.CompanyRepository
	RecruiterRepository repository.RecruiterRepository
}

func NewCompanyFactory(db *gorm.DB) *CompanyFactory {
	return &CompanyFactory{
		CompanyRepository:   repository.NewCompanyRepositoryImplSql(db),
		RecruiterRepository: repository.NewRecruiterRepositoryImplSql(db),
	}
}
