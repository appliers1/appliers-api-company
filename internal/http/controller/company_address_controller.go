package controller

import (
	"appliers/api-company/internal/factory"
	"appliers/api-company/internal/model/dto"
	service "appliers/api-company/internal/service/address"
	"strconv"

	res "appliers/api-company/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type CompanyAddressController struct {
	CompanyAddressService service.CompanyAddressService
}

func NewCompanyAddressController(f *factory.CompanyAddressFactory) *CompanyAddressController {
	return &CompanyAddressController{
		CompanyAddressService: service.Newservice(f),
	}
}

func (controller *CompanyAddressController) Create(c echo.Context) error {
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errCompanyId != nil {
		return errCompanyId
	}

	payload := new(dto.CompanyAddressRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CompanyAddressService.Create(c.Request().Context(), payload, uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CompanyAddressController) Update(c echo.Context) error {
	payload := new(dto.UpdateCompanyAddressRequestBody)
	err := c.Bind(payload)

	id, errId := strconv.ParseUint(c.Param("address_id"), 10, 32)
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)

	if err != nil || errId != nil || errCompanyId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.CompanyAddressService.Update(c.Request().Context(), payload, uint(id), uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CompanyAddressController) Delete(c echo.Context) error {
	id, errId := strconv.ParseUint(c.Param("address_id"), 10, 32)
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errId != nil || errCompanyId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	err := controller.CompanyAddressService.Delete(c.Request().Context(), uint(id), uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *CompanyAddressController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("address_id"), 10, 32)
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if err != nil || errCompanyId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	user, err := controller.CompanyAddressService.FindById(c.Request().Context(), uint(id), uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *CompanyAddressController) Find(c echo.Context) error {
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errCompanyId != nil {
		return errCompanyId
	}

	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.CompanyAddressService.Find(c.Request().Context(), payload, uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get company addresses success", nil).Send(c)
}
