package controller

import (
	"appliers/api-company/internal/factory"
	"appliers/api-company/internal/model/dto"
	service "appliers/api-company/internal/service/recruiter"
	"strconv"

	"appliers/api-company/pkg/constant"
	res "appliers/api-company/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type RecruiterController struct {
	RecruiterService service.RecruiterService
}

func NewRecruiterController(f *factory.RecruiterFactory) *RecruiterController {
	return &RecruiterController{
		RecruiterService: service.Newservice(f),
	}
}

func (controller *RecruiterController) Create(c echo.Context) error {
	payload := new(dto.RecruiterRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	recruiter, err := controller.RecruiterService.Create(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(recruiter).Send(c)
}

func (controller *RecruiterController) Update(c echo.Context) error {
	payload := new(dto.UpdateRecruiterRequestBody)
	err := c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	recruiter, err := controller.RecruiterService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(recruiter).Send(c)
}

func (controller *RecruiterController) Delete(c echo.Context) error {
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errCompanyId != nil {
		return errCompanyId
	}

	var id, errId = strconv.ParseUint(c.Param("id"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	err := controller.RecruiterService.Delete(c.Request().Context(), uint(id), uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *RecruiterController) FindById(c echo.Context) error {
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errCompanyId != nil {
		return errCompanyId
	}

	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	recruiter, err := controller.RecruiterService.FindById(c.Request().Context(), uint(id), uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(recruiter).Send(c)
}

func (controller *RecruiterController) Find(c echo.Context) error {
	companyId, errCompanyId := strconv.ParseUint(c.Param("company_id"), 10, 32)
	if errCompanyId != nil {
		return errCompanyId
	}

	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.RecruiterService.Find(c.Request().Context(), payload, uint(companyId))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get company addresses success", nil).Send(c)
}

func (controller *RecruiterController) FindByUserId(c echo.Context) error {
	var userId, err = strconv.ParseUint(c.Param("user_id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	recruiter, err := controller.RecruiterService.FindByUserId(c.Request().Context(), uint(userId))
	if err != nil {
		if err == constant.RecordNotFound {
			res.ErrorBuilder(&res.ErrorConstant.NotFound, err).Send(c)
		}
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(recruiter).Send(c)
}
