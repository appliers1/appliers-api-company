package router

import (
	"appliers/api-company/internal/http/controller"
	"appliers/api-company/internal/http/middleware"
	. "appliers/api-company/pkg/constant"

	"github.com/labstack/echo/v4"
)

func CompanyAddressRouter(e *echo.Echo, c *controller.CompanyAddressController) {
	e.GET("/companies/:company_id/addresses", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.POST("/companies/:company_id/addresses", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.GET("/companies/:company_id/addresses/:address_id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.PATCH("/companies/:company_id/addresses/:address_id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.DELETE("/companies/:company_id/addresses/:address_id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
}
