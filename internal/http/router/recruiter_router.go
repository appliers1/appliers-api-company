package router

import (
	"appliers/api-company/internal/http/controller"
	"appliers/api-company/internal/http/middleware"
	. "appliers/api-company/pkg/constant"

	"github.com/labstack/echo/v4"
)

func RecruiterRouter(e *echo.Echo, c *controller.RecruiterController) {
	e.POST("/recruiters", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.GET("/recruiters/users/:user_id", c.FindByUserId, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)

	e.GET("/companies/:company_id/recruiters", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.GET("/companies/:company_id/recruiters/:id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.PATCH("/companies/:company_id/recruiters/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.DELETE("/companies/:company_id/recruiters/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
