package router

import (
	"appliers/api-company/internal/http/controller"
	"appliers/api-company/internal/http/middleware"
	. "appliers/api-company/pkg/constant"

	"github.com/labstack/echo/v4"
)

func CompanyRouter(e *echo.Echo, c *controller.CompanyController) {
	e.GET("/companies", c.Find, middleware.NewAllowedRole(AllRole).Authentication)
	e.POST("/companies", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.GET("/companies/:id", c.FindById, middleware.NewAllowedRole(AllRole).Authentication)
	e.PATCH("/companies/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.DELETE("/companies/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
