package http

import (
	"appliers/api-company/internal/factory"
	c "appliers/api-company/internal/http/controller"
	"appliers/api-company/internal/http/router"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func Init(e *echo.Echo, db *gorm.DB) {
	router.CompanyRouter(e, c.NewCompanyController(factory.NewCompanyFactory(db)))
	router.CompanyAddressRouter(e, c.NewCompanyAddressController(factory.NewCompanyAddressFactory(db)))
	router.RecruiterRouter(e, c.NewRecruiterController(factory.NewRecruiterFactory(db)))
}
