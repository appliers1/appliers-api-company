package address

import (
	"appliers/api-company/internal/factory"
	_mockRepository "appliers/api-company/internal/mocks/repository"
	"appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/util/jwtpayload"
	"appliers/api-company/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var companyRepository _mockRepository.CompanyRepository
var companyAddressRepository _mockRepository.CompanyAddressRepository
var ctx context.Context
var companyAddressService CompanyAddressService
var companyAddressFactory = factory.CompanyAddressFactory{
	CompanyRepository:        &companyRepository,
	CompanyAddressRepository: &companyAddressRepository,
}
var companyAddressDomain domain.CompanyAddress
var err error

func setup() {
	companyAddressService = Newservice(&companyAddressFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	companyAddressDomain = domain.CompanyAddress{
		CompanyID:  1,
		Address:    "Jalan Tegal Rotan No 78, Bintaro Sektor 9 Tangerang Selatan",
		PostalCode: "15413",
	}
}

func TestDelete(t *testing.T) {
	setup()
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyAddressRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := companyAddressService.Delete(ctx, uint(1), uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	companyAddressRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := companyAddressService.Delete(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyAddressRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&companyAddressDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		companyAddress, _ := companyAddressService.FindById(ctx, uint(1), uint(1))
		assert.Equal(t, companyAddress.CompanyAddress.Address, companyAddressDomain.Address)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	companyAddressRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 5 | Invalid FindById", func(t *testing.T) {
		_, err := companyAddressService.FindById(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	companyAddressUpdate := domain.CompanyAddress{
		CompanyID:  1,
		Address:    "Mars",
		PostalCode: "15413",
		Telp:       "084484048",
	}
	dto := dto.UpdateCompanyAddressRequestBody{
		Address:    &companyAddressUpdate.Address,
		PostalCode: &companyAddressUpdate.PostalCode,
		Telp:       &companyAddressUpdate.Telp,
	}
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyAddressRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&companyAddressUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		companyAddress, _ := companyAddressService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotEqual(t, companyAddress.CompanyAddress.Address, companyAddressDomain.Address)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	companyAddressUpdate := domain.CompanyAddress{
		CompanyID:  1,
		Address:    "Mars",
		PostalCode: "15413",
		Telp:       "084484048",
	}
	dto := dto.UpdateCompanyAddressRequestBody{
		Address:    &companyAddressUpdate.Address,
		PostalCode: &companyAddressUpdate.PostalCode,
		Telp:       &companyAddressUpdate.Telp,
	}
	companyAddressRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	companyAddressRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {

		_, err := companyAddressService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.CompanyAddressRequestBody{
		CompanyID:  companyAddressDomain.CompanyID,
		Address:    companyAddressDomain.Address,
		PostalCode: companyAddressDomain.PostalCode,
		Telp:       companyAddressDomain.Telp,
	}
	companyAddressRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&companyAddressDomain, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		companyAddress, _ := companyAddressService.Create(ctx, &dto, uint(1))
		assert.Equal(t, companyAddress.CompanyAddress.Address, companyAddressDomain.Address)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.CompanyAddressRequestBody{
		CompanyID:  companyAddressDomain.CompanyID,
		Address:    companyAddressDomain.Address,
		PostalCode: companyAddressDomain.PostalCode,
		Telp:       companyAddressDomain.Telp,
	}
	companyAddressRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Create", func(t *testing.T) {
		_, err := companyAddressService.Create(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.CompanyAddress
	data = append(data, companyAddressDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	companyAddressRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "Tegal",
		}
		companyAddresss, _ := companyAddressService.Find(ctx, &dto, uint(1))
		assert.Equal(t, companyAddresss.Datas[0].CompanyAddress.Address, companyAddressDomain.Address)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	companyAddressRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "Tegal",
		}
		_, err := companyAddressService.Find(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}
