package address

import (
	"appliers/api-company/internal/factory"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/util/compare"
	"context"

	repository "appliers/api-company/internal/repository"
	res "appliers/api-company/pkg/util/response"
)

type CompanyAddressService interface {
	Create(ctx context.Context, payload *dto.CompanyAddressRequestBody, companyId uint) (*dto.CompanyAddressResponse, error)
	Update(ctx context.Context, request *dto.UpdateCompanyAddressRequestBody, companyAddressId uint, companyId uint) (*dto.CompanyAddressResponse, error)
	Delete(ctx context.Context, companyAddressId uint, companyId uint) error
	FindById(ctx context.Context, companyAddressId uint, companyId uint) (*dto.CompanyAddressResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest, companyId uint) (*dto.SearchGetResponse[dto.CompanyAddressResponse], error)
}

type service struct {
	CompanyAddressRepository repository.CompanyAddressRepository
	CompanyRepository        repository.CompanyRepository
}

func Newservice(f *factory.CompanyAddressFactory) CompanyAddressService {
	return &service{
		CompanyAddressRepository: f.CompanyAddressRepository,
		CompanyRepository:        f.CompanyRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.CompanyAddressRequestBody, CompanyID uint) (*dto.CompanyAddressResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, CompanyID) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	data, err := s.CompanyAddressRepository.Save(ctx, ToCompanyAddressDomain(payload))

	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	} else {
		result := dto.CompanyAddressResponse{
			CompanyAddress: *data,
		}
		return &result, nil
	}
}

func (s *service) Update(ctx context.Context, payload *dto.UpdateCompanyAddressRequestBody, companyAddressId uint, companyId uint) (*dto.CompanyAddressResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.CompanyAddressRepository.CountById(ctx, companyAddressId)
	if err == nil {
		if count == 1 {

			data, err := s.CompanyAddressRepository.Update(ctx, UpdateToCompanyAddressDomain(payload), companyAddressId)

			if err == nil {
				data.ID = companyAddressId
				companyResponse := ToCompanyAddressResponse(*data)
				return &companyResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, companyAddressId uint, companyId uint) error {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.CompanyAddressRepository.CountById(ctx, companyAddressId)
	if err == nil {
		if count == 1 {
			err := s.CompanyAddressRepository.Delete(ctx, companyAddressId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, companyAddressId uint, companyId uint) (*dto.CompanyAddressResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	count, err := s.CompanyAddressRepository.CountById(ctx, companyAddressId)
	if err == nil {
		if count == 1 {
			company, err := s.CompanyAddressRepository.FindById(ctx, companyAddressId)
			if err == nil {
				companyResponse := ToCompanyAddressResponse(*company)
				return &companyResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest, companyId uint) (*dto.SearchGetResponse[dto.CompanyAddressResponse], error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	companys, info, err := s.CompanyAddressRepository.FindAll(ctx, payload, companyId, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.CompanyAddressResponse

	for _, company := range companys {
		datas = append(datas, ToCompanyAddressResponse(company))
	}

	result := new(dto.SearchGetResponse[dto.CompanyAddressResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}
