package address

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
)

func ToCompanyAddressResponse(address CompanyAddress) dto.CompanyAddressResponse {
	return dto.CompanyAddressResponse{
		CompanyAddress: address,
	}
}

func ToCompanyAddressDomain(req *dto.CompanyAddressRequestBody) *CompanyAddress {
	address := CompanyAddress{
		CompanyID:  req.CompanyID,
		Address:    req.Address,
		PostalCode: req.PostalCode,
		Telp:       req.Telp,
	}
	return &address
}

func UpdateToCompanyAddressDomain(req *dto.UpdateCompanyAddressRequestBody) *CompanyAddress {
	address := CompanyAddress{}

	if req.Address != nil {
		address.Address = *req.Address
	}

	if req.PostalCode != nil {
		address.PostalCode = *req.PostalCode
	}

	if req.Telp != nil {
		address.Telp = *req.Telp
	}

	return &address
}
