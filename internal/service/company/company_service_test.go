package company

import (
	"appliers/api-company/internal/factory"
	_mockRepository "appliers/api-company/internal/mocks/repository"
	"appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/util/jwtpayload"
	"appliers/api-company/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var companyRepository _mockRepository.CompanyRepository
var recruiterRepository _mockRepository.RecruiterRepository
var ctx context.Context
var companyService CompanyService
var companyFactory = factory.CompanyFactory{
	CompanyRepository:   &companyRepository,
	RecruiterRepository: &recruiterRepository,
}
var companyDomain domain.Company
var err error

func setup() {
	companyService = Newservice(&companyFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	companyDomain = domain.Company{
		Name:           "Data On",
		Service:        "HR Service",
		About:          "DataOn is the leading provider of HR, ERP, and Accounting technology solutions trusted since 1999.",
		Logo:           "url",
		TotalEmployees: 500,
		Website:        "https://dataon.com/",
		Email:          "admin@dataon.com",
		CompanyAddresses: []domain.CompanyAddress{
			{
				CompanyID:  1,
				Address:    "Jalan Tegal Rotan No 78, Bintaro Sektor 9 Tangerang Selatan",
				PostalCode: "15413",
			},
		},
		Recruiters: []domain.Recruiter{
			{
				UserID:    2,
				CompanyID: 1,
				Name:      "Galih",
			},
		},
	}
}

func TestDelete(t *testing.T) {
	setup()
	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := companyService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	companyRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := companyService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&companyDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		company, _ := companyService.FindById(context.Background(), uint(1))
		assert.Equal(t, company.Email, companyDomain.Email)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	companyRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := companyService.FindById(context.Background(), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	companyUpdate := domain.Company{
		Name:           "Data On 1",
		Service:        "HR Service",
		About:          "DataOn is the leading provider of HR, ERP, and Accounting technology solutions trusted since 1999.",
		Logo:           "url",
		TotalEmployees: 500,
		Website:        "https://dataon.com/",
		Email:          "admin@dataon.com",
	}
	dto := dto.UpdateCompanyRequestBody{
		Name:    &companyUpdate.Name,
		Service: &companyUpdate.Service,
	}
	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	companyRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&companyDomain, nil).Once()
	companyRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&companyUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		company, _ := companyService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, company.Name, companyDomain.Name)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	companyUpdate := domain.Company{
		Name:           "Data On 1",
		Service:        "HR Service",
		About:          "DataOn is the leading provider of HR, ERP, and Accounting technology solutions trusted since 1999.",
		Logo:           "url",
		TotalEmployees: 500,
		Website:        "https://dataon.com/",
		Email:          "admin@dataon.com",
	}
	dto := dto.UpdateCompanyRequestBody{
		Name:    &companyUpdate.Name,
		Service: &companyUpdate.Service,
	}

	companyRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	companyRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()
	companyRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {
		_, err := companyService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.CompanyRequestBody{
		Name:           companyDomain.Name,
		Service:        companyDomain.Service,
		About:          companyDomain.About,
		Logo:           companyDomain.Logo,
		TotalEmployees: companyDomain.TotalEmployees,
		Website:        companyDomain.Website,
		Email:          companyDomain.Email,
	}
	companyRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&companyDomain, nil).Once()
	recruiterRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		company, _ := companyService.Create(ctx, &dto)
		assert.Equal(t, company.Name, companyDomain.Name)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.CompanyRequestBody{
		Name:           companyDomain.Name,
		Service:        companyDomain.Service,
		About:          companyDomain.About,
		Logo:           companyDomain.Logo,
		TotalEmployees: companyDomain.TotalEmployees,
		Website:        companyDomain.Website,
		Email:          companyDomain.Email,
	}
	companyRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()
	recruiterRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		_, err := companyService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Company
	data = append(data, companyDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	companyRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "data",
		}
		companies, _ := companyService.Find(context.Background(), &dto)
		assert.Equal(t, companies.Datas[0].Email, companyDomain.Email)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	companyRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "data",
		}
		_, err := companyService.Find(context.Background(), &dto)
		assert.NotNil(t, err)
	})
}
