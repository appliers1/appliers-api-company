package company

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/service/recruiter"
)

func ToCompanyResponse(company Company) dto.CompanyResponse {

	var recruiters = []dto.RecruiterResponse{}
	for _, r := range company.Recruiters {
		recruiters = append(recruiters, recruiter.ToRecruiterResponse(r))
	}

	return dto.CompanyResponse{
		ID:               company.ID,
		Name:             company.Name,
		Service:          company.Service,
		About:            company.About,
		Logo:             company.Logo,
		TotalEmployees:   company.TotalEmployees,
		Website:          company.Website,
		Email:            company.Email,
		CompanyAddresses: company.CompanyAddresses,
		Recruiters:       recruiters,
	}
}

func ToCompanyDomain(req *dto.CompanyRequestBody) *Company {
	company := Company{
		Name:           req.Name,
		Service:        req.Service,
		About:          req.About,
		Logo:           req.Logo,
		TotalEmployees: req.TotalEmployees,
		Website:        req.Website,
		Email:          req.Email,
	}
	return &company
}

func UpdateToCompanyDomain(req *dto.UpdateCompanyRequestBody) *Company {
	company := Company{}

	if req.Name != nil {
		company.Name = *req.Name
	}

	if req.Service != nil {
		company.Service = *req.Service
	}

	if req.About != nil {
		company.About = *req.About
	}

	if req.Logo != nil {
		company.Logo = *req.Logo
	}

	if req.TotalEmployees != nil {
		company.TotalEmployees = *req.TotalEmployees
	}

	if req.Website != nil {
		company.Website = *req.Website
	}

	if req.Email != nil {
		company.Email = *req.Email
	}

	return &company
}
