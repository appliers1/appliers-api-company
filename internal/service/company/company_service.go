package company

import (
	"appliers/api-company/internal/factory"
	"appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/util/compare"
	"appliers/api-company/internal/util/jwtpayload"
	"context"

	repository "appliers/api-company/internal/repository"
	"appliers/api-company/pkg/constant"
	res "appliers/api-company/pkg/util/response"
)

type CompanyService interface {
	Create(ctx context.Context, payload *dto.CompanyRequestBody) (*dto.CompanyResponse, error)
	Update(ctx context.Context, request *dto.UpdateCompanyRequestBody, companyId uint) (*dto.CompanyResponse, error)
	Delete(ctx context.Context, companyId uint) error
	FindById(ctx context.Context, companyId uint) (*dto.CompanyResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CompanyResponse], error)
}

type service struct {
	CompanyRepository   repository.CompanyRepository
	RecruiterRepository repository.RecruiterRepository
}

func Newservice(f *factory.CompanyFactory) CompanyService {
	return &service{
		CompanyRepository:   f.CompanyRepository,
		RecruiterRepository: f.RecruiterRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.CompanyRequestBody) (*dto.CompanyResponse, error) {

	data, err := s.CompanyRepository.Save(ctx, ToCompanyDomain(payload))

	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	} else {

		cc := ctx.Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
		if cc.Role == constant.ROLE_COMPANY {
			recruiter := domain.Recruiter{
				CompanyID: data.ID,
			}
			_, err := s.RecruiterRepository.Update(ctx, &recruiter, uint(cc.PersonId))
			if err != nil {
				s.CompanyRepository.Delete(ctx, data.ID)
				return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
			}
		}

		result := ToCompanyResponse(*data)
		return &result, nil
	}
}

func (s *service) Update(ctx context.Context, request *dto.UpdateCompanyRequestBody, companyId uint) (*dto.CompanyResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	count, err := s.CompanyRepository.CountById(ctx, companyId)
	if err == nil {
		if count == 1 {

			data, err := s.CompanyRepository.Update(ctx, UpdateToCompanyDomain(request), companyId)

			if err == nil {
				data.ID = companyId
				companyResponse := ToCompanyResponse(*data)
				return &companyResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, companyId uint) error {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.CompanyRepository.CountById(ctx, companyId)
	if err == nil {
		if count == 1 {
			err := s.CompanyRepository.Delete(ctx, companyId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, companyId uint) (*dto.CompanyResponse, error) {
	count, err := s.CompanyRepository.CountById(ctx, companyId)
	if err == nil {
		if count == 1 {
			company, err := s.CompanyRepository.FindById(ctx, companyId)
			if err == nil {
				companyResponse := ToCompanyResponse(*company)
				return &companyResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.CompanyResponse], error) {

	companys, info, err := s.CompanyRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.CompanyResponse

	for _, company := range companys {
		datas = append(datas, ToCompanyResponse(company))
	}

	result := new(dto.SearchGetResponse[dto.CompanyResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}
