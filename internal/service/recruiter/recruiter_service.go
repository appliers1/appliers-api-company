package recruiter

import (
	"appliers/api-company/internal/factory"
	"appliers/api-company/internal/model/dto"
	"context"

	repository "appliers/api-company/internal/repository"
	"appliers/api-company/internal/util/compare"
	res "appliers/api-company/pkg/util/response"
)

type RecruiterService interface {
	Create(ctx context.Context, payload *dto.RecruiterRequestBody) (*dto.RecruiterResponse, error)
	Update(ctx context.Context, request *dto.UpdateRecruiterRequestBody, recruiterId uint) (*dto.RecruiterResponse, error)
	Delete(ctx context.Context, recruiterId uint, companyId uint) error
	FindById(ctx context.Context, recruiterId uint, companyId uint) (*dto.RecruiterResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest, companyId uint) (*dto.SearchGetResponse[dto.RecruiterResponse], error)
	FindByUserId(ctx context.Context, userId uint) (*dto.RecruiterResponse, error)
}

type service struct {
	RecruiterRepository repository.RecruiterRepository
	CompanyRepository   repository.CompanyRepository
}

func Newservice(f *factory.RecruiterFactory) RecruiterService {
	return &service{
		RecruiterRepository: f.RecruiterRepository,
		CompanyRepository:   f.CompanyRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.RecruiterRequestBody) (*dto.RecruiterResponse, error) {
	data, err := s.RecruiterRepository.Save(ctx, ToRecruiterDomain(payload))

	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	} else {
		response := ToRecruiterResponse(*data)
		return &response, nil
	}
}

func (s *service) Update(ctx context.Context, request *dto.UpdateRecruiterRequestBody, recruiterId uint) (*dto.RecruiterResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, *request.CompanyID) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.RecruiterRepository.CountById(ctx, recruiterId)
	if err == nil {
		if count == 1 {

			data, err := s.RecruiterRepository.Update(ctx, UpdateToRecruiterDomain(request), recruiterId)

			if err == nil {
				data.ID = recruiterId
				recruiterResponse := ToRecruiterResponse(*data)
				return &recruiterResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Delete(ctx context.Context, recruiterId uint, companyId uint) error {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.RecruiterRepository.CountById(ctx, recruiterId)
	if err == nil {
		if count == 1 {
			err := s.RecruiterRepository.Delete(ctx, recruiterId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, recruiterId uint, companyId uint) (*dto.RecruiterResponse, error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	count, err := s.RecruiterRepository.CountById(ctx, recruiterId)
	if err == nil {
		if count == 1 {
			recruiter, err := s.RecruiterRepository.FindById(ctx, recruiterId, companyId)
			if err == nil {
				recruiterResponse := ToRecruiterResponse(*recruiter)
				return &recruiterResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest, companyId uint) (*dto.SearchGetResponse[dto.RecruiterResponse], error) {
	// Pass if user's company equal company data value
	if !compare.CompareCompanyID(ctx, companyId) {
		return nil, res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	recruiters, info, err := s.RecruiterRepository.FindAll(ctx, payload, &payload.Pagination, companyId)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.RecruiterResponse

	for _, recruiter := range recruiters {
		datas = append(datas, ToRecruiterResponse(recruiter))
	}

	result := new(dto.SearchGetResponse[dto.RecruiterResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}

func (s *service) FindByUserId(ctx context.Context, userId uint) (*dto.RecruiterResponse, error) {
	recruiter, err := s.RecruiterRepository.FindByUserId(ctx, userId)
	if err != nil {
		return nil, err
	}
	recruiterResponse := ToRecruiterResponse(*recruiter)
	return &recruiterResponse, nil
}
