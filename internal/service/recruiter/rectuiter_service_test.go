package recruiter

import (
	"appliers/api-company/internal/factory"
	_mockRepository "appliers/api-company/internal/mocks/repository"
	"appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
	"appliers/api-company/internal/util/jwtpayload"
	"appliers/api-company/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var companyRepository _mockRepository.CompanyRepository
var recruiterRepository _mockRepository.RecruiterRepository
var ctx context.Context
var recruiterService RecruiterService
var recruiterFactory = factory.RecruiterFactory{
	CompanyRepository:   &companyRepository,
	RecruiterRepository: &recruiterRepository,
}
var recruiterDomain domain.Recruiter
var err error

func setup() {
	recruiterService = Newservice(&recruiterFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	recruiterDomain = domain.Recruiter{
		UserID:    2,
		CompanyID: 1,
		Name:      "Galih",
	}
}

func TestDelete(t *testing.T) {
	setup()
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	recruiterRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := recruiterService.Delete(ctx, uint(1), uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	recruiterRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := recruiterService.Delete(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	recruiterRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(&recruiterDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		recruiter, _ := recruiterService.FindById(ctx, uint(1), uint(1))
		assert.Equal(t, recruiter.Name, recruiterDomain.Name)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	recruiterRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 5 | Invalid FindById", func(t *testing.T) {
		_, err := recruiterService.FindById(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	recruiterUpdate := domain.Recruiter{
		UserID:    2,
		CompanyID: 1,
		Name:      "Bagas",
	}
	dto := dto.UpdateRecruiterRequestBody{
		UserID:    &recruiterUpdate.UserID,
		CompanyID: &recruiterUpdate.CompanyID,
		Name:      &recruiterUpdate.Name,
	}
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	recruiterRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&recruiterUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		recruiter, _ := recruiterService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, recruiter.Name, recruiterDomain.Name)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	recruiterUpdate := domain.Recruiter{
		UserID:    2,
		CompanyID: 1,
		Name:      "Bagas",
	}
	dto := dto.UpdateRecruiterRequestBody{
		UserID:    &recruiterUpdate.UserID,
		CompanyID: &recruiterUpdate.CompanyID,
		Name:      &recruiterUpdate.Name,
	}
	recruiterRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	recruiterRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		_, err := recruiterService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	dto := dto.RecruiterRequestBody{
		UserID:    recruiterDomain.UserID,
		CompanyID: &recruiterDomain.CompanyID,
		Name:      recruiterDomain.Name,
	}
	recruiterRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(&recruiterDomain, nil).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		company, _ := recruiterService.Create(ctx, &dto)
		assert.Equal(t, company.Name, recruiterDomain.Name)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.RecruiterRequestBody{
		UserID:    recruiterDomain.UserID,
		CompanyID: &recruiterDomain.CompanyID,
		Name:      recruiterDomain.Name,
	}
	recruiterRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		_, err := recruiterService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Recruiter
	data = append(data, recruiterDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	recruiterRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "Gali",
		}
		recruiters, _ := recruiterService.Find(ctx, &dto, uint(1))
		assert.Equal(t, recruiters.Datas[0].Name, recruiterDomain.Name)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	var data []domain.Recruiter
	data = append(data, recruiterDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	recruiterRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "Gali",
		}
		_, err := recruiterService.Find(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}
