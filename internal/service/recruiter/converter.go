package recruiter

import (
	. "appliers/api-company/internal/model/domain"
	"appliers/api-company/internal/model/dto"
)

func ToRecruiterResponse(recruiter Recruiter) dto.RecruiterResponse {
	var recruiterResponse = dto.RecruiterResponse{
		ID:        recruiter.ID,
		CompanyID: recruiter.CompanyID,
		UserID:    recruiter.UserID,
		Name:      recruiter.Name,
	}
	return recruiterResponse
}

func ToRecruiterDomain(req *dto.RecruiterRequestBody) *Recruiter {

	recruiter := Recruiter{
		UserID: req.UserID,
		Name:   req.Name,
	}

	if req.CompanyID != nil {
		recruiter.CompanyID = *req.CompanyID
	}

	return &recruiter
}

func UpdateToRecruiterDomain(req *dto.UpdateRecruiterRequestBody) *Recruiter {
	recruiter := Recruiter{}

	if req.CompanyID != nil {
		recruiter.CompanyID = *req.CompanyID
	}

	if req.UserID != nil {
		recruiter.UserID = *req.UserID
	}

	if req.Name != nil {
		recruiter.Name = *req.Name
	}

	return &recruiter
}
