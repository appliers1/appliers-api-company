#stageI (Build Binary)
FROM golang:1.18.3-alpine3.16 AS builder
WORKDIR /app
COPY ./ ./
RUN go mod download
# RUN /bin/bash -c 'go run main.go -migrate=migrate; /bin/bash'
CMD /bin/bash -c 'go run main.go -migrate=migrate; /bin/bash'
RUN go build -o main

#stageII
FROM alpine:3.16
WORKDIR /app
COPY --from=builder /app/main .
COPY --from=builder /app/.env .
EXPOSE 8002
CMD [ "./main" ]